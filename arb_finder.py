import utils


def find_arb_opportunities(exchanges):
    """
    splits games into bet types then looks for arb opportunities in each
    """
    nested_dict = nest_on_bet_type(exchanges)

    arb_opportunities = []
    games_compared = 0
    for _, value in nested_dict.items():
        arbs, num_games = cross_compare(value)
        arb_opportunities += arbs
        games_compared += num_games

    return arb_opportunities, games_compared


def nest_on_bet_type(exchanges):
    nest_on_bet_type = {
        "Match": {},
        "Handicap": {},
        "Over/Under": {}
    }
    for exchange in exchanges:
        for line in exchange:
            game_id = line["game_id"]
            bet_type = line["bet_type"]

            # create dictonary item if it doesnt exist
            if game_id not in nest_on_bet_type[bet_type]:
                nest_on_bet_type[bet_type][game_id] = []

            nest_on_bet_type[bet_type][game_id].append(line)
    return nest_on_bet_type


def calculate_bet_ratios(input):
    """
    input is an arb dict with implied probabilities

    calculates the ratios bet bets to be made
    """

    home_odds = utils.implied_to_decimal(input["home_team_odds"])
    away_odds = utils.implied_to_decimal(input["away_team_odds"])

    smallest = min(home_odds, away_odds)

    home_bet_ratio = home_odds / smallest
    away_bet_ratio = away_odds / smallest

    result = {
        "home_bet_ratio": home_bet_ratio,
        "away_bet_ratio": away_bet_ratio,
    }

    return result


def cross_compare(lines):
    """
    cross products all the games in lines and looks for arbs
    lines is a dict with keys of game_ids and values that are lists of games from exchanges    
    """
    games_compared = 0
    arb_opportunities = []

    for game_id, game_list in lines.items():
        for i in range(len(game_list)):
            for j in range(len(game_list)):

                # dont check for arb on the same exchange
                # and ignore if the handicap or over/under meta is different
                if (game_list[i] == game_list[j]):
                    continue
                if (game_list[i]["meta"] != game_list[j]["meta"]):
                    continue
                games_compared += 1

                if (game_list[i]["home_team_odds"] + game_list[j]["away_team_odds"] < 1):
                    assert(game_list[i]["bet_type"] ==
                           game_list[j]["bet_type"])

                    arb_opp = {
                        "game_id": game_id,
                        "bet_type": game_list[i]["bet_type"],
                        "home_team_exchange": game_list[i]["book"],
                        "away_team_exchange": game_list[j]["book"],
                        "home_team": game_list[i]["home_team"],
                        "away_team": game_list[j]["away_team"],
                        "home_team_link": game_list[i]["link"],
                        "away_team_link": game_list[j]["link"],
                        "home_team_odds": game_list[i]["home_team_odds"],
                        "away_team_odds": game_list[j]["away_team_odds"],
                        "take": game_list[i]["home_team_odds"] + game_list[j]["away_team_odds"],
                        "meta": game_list[i]["meta"]
                    }

                    bet_ratios = calculate_bet_ratios(arb_opp)
                    arb_opp = {**arb_opp, **bet_ratios}

                    arb_opportunities.append(arb_opp)

    return arb_opportunities, games_compared
