import datetime


class Exchange(object):

    def get_odds(self):
        raise Exception('implement me pls')

    def format_events(self, odd):
        raise Exception('implement me pls')

    def format_game_time_date(self, game_time):
        raise Exception('implement me pls')

    def generate_game_id(self, game_time, home_team, away_team):
        """
        requires game_time to be unix time
        and that home / away team names are the three letter team codes
        """
        game_id = str(game_time) + "_" + home_team + "_" + away_team
        return game_id
