"""
handicap meta is tracked for home team
"""
import requests
from base_exchange import Exchange
import utils


class Bodog(Exchange):

    def get_odds(self):
        events = self.fetch_events()
        odds, number_of_games = self.parse_events_for_odds(events)
        return odds, number_of_games

    def fetch_events(self):
        url = 'https://www.bodog.eu/services/sports/event/v2/events/A/description/baseball/mlb'
        params = {
            'marketFilterId': 'def',
            'preMatchOnly': True
        }
        data = requests.get(url, params=params).json()
        events = data[0]["events"]
        return events

    def parse_events_for_odds(self, events):
        results = []
        base_url = "https://www.bodog.eu/sports"
        number_of_games = 0
        for event in events:

            link = base_url + event["link"]
            # divide by 1000 because time is given in unix milliseconds

            unix_game_time = int(event["startTime"]/1000)
            teams_string = event["description"]
            teams = teams_string.split("@")
            # slicing team name string below to remove white space
            game_id = self.generate_game_id(
                unix_game_time,
                self.name_and_code[teams[1][1:]],
                self.name_and_code[teams[0][:-1]]
            )

            result = {
                "book": "bodog",
                "game_id": game_id,
                "league": "mlb",
                "game_time": unix_game_time,
                "link": link,
            }
            number_of_games += 1
            display_groups = event['displayGroups']
            for dg in display_groups:
                markets = dg['markets']
                for m in markets:
                    # convert bet names to the standard
                    bet = m['description']
                    if bet == "Runline":
                        bet = "Handicap"
                    elif bet == "Moneyline":
                        bet = "Match"
                    else:
                        bet = "Over/Under"
                    outcomes = m['outcomes']

                    result["bet_type"] = bet

                    if not outcomes:
                        continue
                    if (bet == "Handicap") or (bet == "Match"):
                        home_team = self.name_and_code[
                            outcomes[1]["description"]]
                        home_team_odds = utils.decimal_to_implied(
                            float(outcomes[1]["price"]["decimal"]))
                        away_team = self.name_and_code[
                            outcomes[0]["description"]]
                        away_team_odds = utils.decimal_to_implied(
                            float(outcomes[0]["price"]["decimal"]))
                        if (bet == "Handicap"):
                            meta = outcomes[1]["price"]["handicap"]
                        else:
                            meta = None

                    else:
                        home_team = "Over"
                        away_team = "Under"
                        home_team_odds = utils.decimal_to_implied(
                            float(outcomes[0]["price"]["decimal"]))
                        away_team_odds = utils.decimal_to_implied(
                            float(outcomes[1]["price"]["decimal"]))
                        meta = outcomes[1]["price"]["handicap"]

                    details = {
                        "home_team": home_team,
                        "home_team_odds": home_team_odds,
                        "away_team": away_team,
                        "away_team_odds": away_team_odds,
                        "meta": float(meta) if meta else None,
                        "take": home_team_odds + away_team_odds

                    }
                    results.append({**result, **details})
        return results, number_of_games

    name_and_code = {"Arizona Diamondbacks": "ARI",
                     "Milwaukee Brewers": "MIL",
                     "Miami Marlins": "MIA",
                     "Oakland Athletics": "OAK",
                     "Cleveland Indians": "CLE",
                     "Boston Red Sox": "BOS",
                     "San Diego Padres": "SDN",
                     "St. Louis Cardinals": "SLN",
                     "ST. Louis Cardinals": "SLN",
                     "Chicago Cubs": "CHN",
                     "Seattle Mariners": "SEA",
                     "Kansas City Royals": "KCA",
                     "Tampa Bay Rays": "TBA",
                     "Colorado Rockies": "COL",
                     "Philadelphia Phillies": "PHI",
                     "New York Mets": "NYN",
                     "Houston Astros": "HOU",
                     "Chicago White Sox": "CHA",
                     "Baltimore Orioles": "BAL",
                     "Los Angeles Dodgers": "LAN",
                     "Cincinnati Reds": "CIN",
                     "Atlanta Braves": "ATL",
                     "Los Angeles Angels": "ANA",
                     "Detroit Tigers": "DET",
                     "New York Yankees": "NYA",
                     "San Francisco Giants": "SFN",
                     "Pittsburgh Pirates": "PIT",
                     "Washington Nationals": "WAS",
                     "Texas Rangers": "TEX",
                     "Minnesota Twins": "MIN",
                     "Toronto Blue Jays": "TOR"}
