"""
grabs bets from intertops.com


order of events:
spread aka handicap
over/under
moneyline aka match

the formatting for intertops is:
lists all games, then lists details for the games separately

currently the way we are lining up games with game details is list index

also game_times list is prepended with 3 blank entries from intertops

the meta listed is home team's

home teams are listed second in the website response
"""

import utils
from datetime import datetime
from dateutil.parser import parse
import dateutil
import requests
from utils import decimal_to_implied as d_to_i
from base_exchange import Exchange
from bs4 import BeautifulSoup
import pytz


class Intertops(Exchange):

    def get_odds(self):
        events = self.fetch_events()
        odds, number_of_games = self.parse_events_for_odds(events)
        return odds, number_of_games

    def fetch_events(self):
        url = "https://sports.intertops.eu/en/Bets/Baseball/MLB-Lines/1524"
        params = {

        }
        res = requests.get(url, params=params)

        soup = BeautifulSoup(res.text, "html.parser")

        return soup

    def format_game_time_date(self, game_time):
        utc = pytz.timezone("UTC")
        date_time = parse(game_time)
        return utc.localize(date_time)

    def get_game_times(self, events):
        times = events.find_all("span", {"class": "eventdatetime"})
        game_times = []
        for item in times:
            raw_game_time = item["title"].replace("<br/>", " ")
            formatted_game_time = self.format_game_time_date(raw_game_time)
            game_times.append(formatted_game_time)
        return game_times

    def get_game_links(self, events):
        block = events.find_all("a", {"class": "seeall"})
        links = []
        for id in block:
            links.append(id["href"])
        return links

    def get_list_of_bets(self, number_of_events):
        bet_order = ["Handicap", "Over/Under", "Match"]
        bets = bet_order * number_of_events
        return bets

    def parse_events_for_odds(self, events):
        results = []
        number_of_games = 0
        base_link = "https://sports.intertops.eu"

        # gets the game times
        raw_game_times = self.get_game_times(events)
        # repeat each time three times
        # because there are three different bet types
        repeated_game_times = [[x, x, x] for x in raw_game_times]
        # flatten the list below
        game_times = [
            item for sublist in repeated_game_times for item in sublist]

        links = self.get_game_links(events)

        # repeats the types of 3 bets for each gametime
        bets = self.get_list_of_bets(len(game_times))
        events = events.find_all(class_="res2")
        # the first three elements of events are not content
        events = events[3:]
        for idx, event in enumerate(events):

            metas = []
            team_info = event.find_all(class_="tablebutton")
            teams = []
            odds = []
            # processes away first, then home
            for item in team_info:
                teams.append(item["title"][:3])
                odd = item.find_all(class_="bs o us-o active")[0]["data-o-inv"]
                odds.append(d_to_i(float(odd)))
                try:
                    metas.append(item.find_all(
                        class_="bs o us-o active")[0]["data-o-pts"])
                except:
                    pass
            if len(teams) == 0:
                continue

            game_time = game_times[idx]
            game_time = int(datetime.timestamp(game_time))

            # since there are 3 bet types, we want each bet type to
            # point to the same links index
            links_index = int(idx/3)
            link = base_link + links[links_index]

            home_team, away_team = teams[1], teams[0]
            home_team_odds, away_team_odds = odds[1], odds[0]

            # use previous game_id if bet is over under
            # since home team and away teams are not listed
            if bets[idx] == "Over/Under":
                home_team = "Over"
                away_team = "Under"
                home_team_odds, away_team_odds = away_team_odds, home_team_odds
            else:
                game_id = self.generate_game_id(
                    game_time,
                    self.name_and_code[home_team],
                    self.name_and_code[away_team]
                )

            result = {
                "book": "intertops",
                "game_id": game_id,
                "league": "mlb",
                "game_time": game_time,
                "bet_type": bets[idx],
                "link": link,
                "home_team": home_team,
                "home_team_odds": home_team_odds,
                "away_team": away_team,
                "away_team_odds": away_team_odds,
                "take": home_team_odds + away_team_odds,
                "meta": float(metas[1]) if metas else None
            }
            results.append(result)
            number_of_games += 1

        return results, number_of_games

    name_and_code = {"LOS": "LAN",
                     "PHI": "PHI",
                     "TAM": "TBA",
                     "NYY": "NYA",
                     "WAS": "WAS",
                     "BAL": "BAL",
                     "SDG": "SDN",
                     "MIA": "MIA",
                     "TOR": "TOR",
                     "BOS": "BOS",
                     "DET": "DET",
                     "CLE": "CLE",
                     "CIN": "CIN",
                     "CHC": "CHN",
                     "ARI": "ARI",
                     "OAK": "OAK",
                     "SEA": "SEA",
                     "KAN": "KCA",
                     "HOU": "HOU",
                     "LAA": "ANA",
                     "SFO": "SFN",
                     "TEX": "TEX",
                     "MIN": "MIN",
                     "PIT": "PIT",
                     "ATL": "ALT",
                     "CWS": "CHA",
                     "NYM": "NYN",
                     "COL": "COL",
                     "STL": "SLN",
                     "MIL": "MIL"
                     }
