"""
call `python main.py slack` to run and send messagses to slack
call `python main.py` to print messages to terminal
"""

from bodog import Bodog
from sport888 import Sport888
from intertops import Intertops
from sports_interaction import Sports_Interaction
import arb_finder
from slack_messaging import slack_messeger
import sys
import utils
import platform
import datetime
import traceback


HEART_BEAT_WINDOW = 15


def load_exchange_odds(exchanges):
    # TODO call exchanges in parallel
    exchange_odds = []
    numbers_of_games = []
    successful_exchanges = []
    unsuccessful_exchanges = []
    for exchange in exchanges:
        try:
            odds, number_of_games = exchange.get_odds()
            exchange_odds.append(odds)
            numbers_of_games.append(number_of_games)
            successful_exchanges.append(exchange)
        except:
            unsuccessful_exchanges.append(exchange)
            continue
    return exchange_odds, numbers_of_games, successful_exchanges, unsuccessful_exchanges


def arb_opp_string_builder(arb_opportunities, alert=False):
    # builds a string to send to slack (or print to terminal)
    string = ""
    host_name = platform.node()
    if arb_opportunities:
        string += ("these are the arb opportunities \n")
        for arb_opp in arb_opportunities:
            string += ("{} \n".format(arb_opp))
        string += ("from : {}".format(host_name))
        if alert:
            string += ("\n <!channel>")
    return string


def heart_beat_string_builder(games_compared,
                              game_in_exchange,
                              successful_exchanges,
                              unsuccessful_exchanges):
    # builds a string to send to slack or print to terminal
    string = ""
    host_name = platform.node()
    string += ("heart beat \n")
    string += ("arb chances compared : {} \n".format(games_compared))
    for idx in range(len(successful_exchanges)):
        exchange_name = successful_exchanges[idx].__class__.__name__
        string += ("{}: {} \n".format(
            exchange_name,
            game_in_exchange[idx]
        ))
    if unsuccessful_exchanges:
        string += "\nunsuccesful exchanges : \n"
        for exchange in unsuccessful_exchanges:
            string += "{} \n".format(exchange.__class__.__name__)
        string += "\n"

    string += ("from : {}".format(host_name))
    return string


def main(print_func):

    # collect and get odds of exchanges
    exchanges_to_load = [
        Bodog(),
        Sport888(),
        Intertops(),
        Sports_Interaction()
    ]

    exchanges, numbers_of_games, successful_exchanges, unsuccessful_exchanges = load_exchange_odds(
        exchanges_to_load)

    # assert exchange result shape
    for exchange in exchanges:
        utils.exchange_result_schema.validate(exchange)

    # compare exchange odds for arbs
    arb_opportunities, games_compared = arb_finder.find_arb_opportunities(
        exchanges)

    # filters out arb opportunities if they are below TAKE_FILTER set in utils.py
    filtered_arb_opportunities = utils.filter_arb_opportunities(
        arb_opportunities)
    # determines if slack should be channel alerted based on TAKE_ALERT
    alert = utils.make_channel_alert(filtered_arb_opportunities)

    time_stamp = datetime.datetime.now()
    # beats heart once an hour - the cron is set to run every 15 mins
    heart_beat = True if time_stamp.minute < HEART_BEAT_WINDOW else False

    # always print arb opportunities
    if filtered_arb_opportunities:
        arb_opportunities_string = arb_opp_string_builder(
            filtered_arb_opportunities,
            alert
        )
        print_func(arb_opportunities_string)

    # print heartbeat independently from arb opportunities
    if heart_beat:
        heart_beat_string = heart_beat_string_builder(
            games_compared,
            numbers_of_games,
            successful_exchanges,
            unsuccessful_exchanges
        )
        if print_func == slack_messeger:
            print_func(heart_beat_string, channel="#dev_testing")
        else:
            print_func(heart_beat_string)


if __name__ == "__main__":

    # default to printing to terminal
    try:
        if sys.argv[1] == "slack":
            print_func = slack_messeger
    except IndexError:
        print_func = print

    # catches all errors and prints them to slack
    try:
        main(print_func)
    except Exception as e:
        error_string = "error : \n"
        error_string += "{} \n".format(traceback.format_exc())
        error_string += "{} \n".format(e)
        error_string += "from : {}".format(platform.node())
        print_func(error_string)
