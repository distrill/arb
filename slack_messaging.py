import os
import slack


def slack_messeger(message, channel="#rich_girl"):

    bot_icon_url = "https://www.gadgetreview.com/wp-content/uploads/2011/08/1990310-loaded-dice-650x730.jpg"

    slack_token = os.environ["SLACK_API_KEY"]
    client = slack.WebClient(token=slack_token)

    response = client.chat_postMessage(
        channel=channel,
        text=message,
        icon_url=bot_icon_url
    )

    assert response["ok"]

    # TODO below errors when there are arbs
    # TODO review if its necessary, or can be removed
    # assert response["message"]["text"] == message
