"""
grabs bets from 888sport.com

the bet types are : match, handicap/runline, total score

handicap and total score have meta fields

uses the meta for the home team
the meta field for handicap is the number of runs to win by
and for total score it is the total number of runs scored

the data comes in two pieces, event data and outcome data (outcomedata is a len 2 list)
home and away team names come from the event data, match outcome data doesnt have team names,
so for match and handicap we are assuming that outcome[0] is for home and outcome[1] is for away
"""

import datetime
from dateutil.parser import parse
import requests
from utils import fractional_to_implied as f_to_i
from base_exchange import Exchange
import pickle


class Sport888(Exchange):

    def get_odds(self):
        events = self.fetch_events()
        odds, number_of_games = self.parse_events_for_odds(events)
        return odds, number_of_games

    def format_game_time_date(self, game_time):
        date_time = parse(game_time)
        return date_time

    def f_to_i(self, frac):
        """
        wrapper for utils.fractional_to_implied
        with some extra specific knowledge for 888sport
        """
        if frac == "Evens":
            return 0.5
        return f_to_i(frac)

    def fetch_events(self):
        url = 'https://eu-offering.kambicdn.org/offering/v2018/888/listView/baseball/mlb.json'
        params = {
            "lang": "en_GB",
            "market": "CA",
            "useCombined": True
        }
        data = requests.get(url, params=params).json()

        return data["events"]

    def parse_events_for_odds(self, events):
        base_link = "https://www.888sport.com/#/filter/baseball/mlb/"
        # Match outcomes dont have team names, but handicap does
        results = []
        number_of_games = 0
        for event in events:

            # get game details
            event_details = event["event"]
            id_for_link = str(event_details["id"])
            home_team = event_details["homeName"]
            away_team = event_details["awayName"]
            # skip if the game has already started
            state = event_details["state"]
            if state == "STARTED":
                continue

            game_time = event_details["start"]
            game_time = self.format_game_time_date((game_time))
            unix_game_time = int(datetime.datetime.timestamp(game_time))

            # get betting details
            bet_offers = event["betOffers"]
            # meta field, value for run line or total score
            line = None
            # assuming that outcomes[0] is always for the home team
            # match outcomes dont have team names, handicap does
            for item in bet_offers:
                bet_type = item["betOfferType"]["name"]
                outcomes = item["outcomes"]

                # game_id is made from unix time of game, and
                # the three letter team codes for home and away
                game_id = self.generate_game_id(
                    unix_game_time,
                    self.name_and_code[home_team],
                    self.name_and_code[away_team])

                result = {
                    "book": "888sport",
                    "game_id": game_id,
                    "league": "mlb",
                    "game_time": unix_game_time,
                    "bet_type": bet_type,
                    "link": base_link + id_for_link
                }

                # line is the total runs for over under / handicap
                # match doesnt have a meta field
                if bet_type != "Match":
                    line, line1 = outcomes[0]["line"], outcomes[1]["line"]
                    # handicap will have +line, -line
                    if abs(line) != abs(line1):
                        print("meta did not match in both outcomes")
                        continue

                # handles over/under differently from match and handicap
                if bet_type == "Over/Under":
                    first_label = outcomes[0]["label"]
                    first_odds = self.f_to_i(
                        outcomes[0]["oddsFractional"])
                    second_label = outcomes[1]["label"]
                    second_odds = self.f_to_i(
                        outcomes[1]["oddsFractional"])
                    details = {
                        "home_team": first_label,
                        "home_team_odds": first_odds,
                        "away_team": second_label,
                        "away_team_odds": second_odds,
                        "take": first_odds + second_odds
                    }
                else:
                    home_team_odds = self.f_to_i(
                        outcomes[0]["oddsFractional"])
                    away_team_odds = self.f_to_i(
                        outcomes[1]["oddsFractional"])
                    details = {
                        "home_team": self.name_and_code[home_team],
                        "home_team_odds": home_team_odds,
                        "away_team": self.name_and_code[away_team],
                        "away_team_odds": away_team_odds,
                        "take": home_team_odds + away_team_odds
                    }

                result = {**result,
                          **details,
                          "meta": (line / 1000.) if line else None}
                results.append(result)
                number_of_games += 1

        return results, number_of_games

    name_and_code = {"Arizona Diamondbacks": "ARI",
                     "Milwaukee Brewers": "MIL",
                     "Miami Marlins": "MIA",
                     "Oakland Athletics": "OAK",
                     "Cleveland Indians": "CLE",
                     "Boston Red Sox": "BOS",
                     "San Diego Padres": "SDN",
                     "St. Louis Cardinals": "SLN",
                     "Chicago Cubs": "CHN",
                     "Seattle Mariners": "SEA",
                     "Kansas City Royals": "KCA",
                     "Tampa Bay Rays": "TBA",
                     "Colorado Rockies": "COL",
                     "Philadelphia Phillies": "PHI",
                     "New York Mets": "NYN",
                     "Houston Astros": "HOU",
                     "Chicago White Sox": "CHA",
                     "Baltimore Orioles": "BAL",
                     "Los Angeles Dodgers": "LAN",
                     "Cincinnati Reds": "CIN",
                     "Atlanta Braves": "ATL",
                     "Los Angeles Angels": "ANA",
                     "Detroit Tigers": "DET",
                     "New York Yankees": "NYA",
                     "San Francisco Giants": "SFN",
                     "Pittsburgh Pirates": "PIT",
                     "Washington Nationals": "WAS",
                     "Texas Rangers": "TEX",
                     "Minnesota Twins": "MIN",
                     "Toronto Blue Jays": "TOR"}
