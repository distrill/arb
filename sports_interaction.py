import requests
from base_exchange import Exchange
from bs4 import BeautifulSoup
import pytz
from dateutil.parser import parse
from datetime import datetime
import utils

"""
sports interaction has three different urls for events
load events for each then merge into one

for handicap we have the hometeam's listed for the meta
"""


class Sports_Interaction(Exchange):

    def get_odds(self):
        events = self.fetch_events()
        odds, number_of_games = self.parse_events_for_odds(events)
        return odds, number_of_games

    def fetch_events(self):
        urls = [
            "https://www.sportsinteraction.com/baseball/national-league-betting-lines/",
            "https://www.sportsinteraction.com/baseball/mlb-betting-lines/",
            "https://www.sportsinteraction.com/baseball/interleague-betting-lines/"
        ]
        responses = [requests.get(url) for url in urls]
        soups = [BeautifulSoup(r.text, "html.parser") for r in responses]

        soup = soups[0]
        for idx in range(1, len(soups)):
            soup.extend(soups[idx])

        return soup

    def format_game_time_date(self, game_time):
        pst = pytz.timezone("America/Toronto")
        date_time = parse(game_time)
        return pst.localize(date_time)

    def convert_odds(self, input_odds):
        odds = float(input_odds)
        return (1 / (odds + 1))

    def parse_events_for_odds(self, events):
        results = []
        number_of_games = 0
        games = events.find_all("div", class_="game")
        for game in games:

            # games are grouped by day
            # so most games wont have a date, so default to previous assignment
            try:
                date = game.find("span", class_="date").getText().strip()
            except AttributeError as e:
                if str(e) != "'NoneType' object has no attribute 'getText'":
                    raise
                else:
                    pass

            # there is a trailing game in games with no game data
            try:
                time = game.find("span", class_="time").getText().strip()
            except AttributeError as e:
                if str(e) != "'NoneType' object has no attribute 'getText'":
                    raise
                else:
                    pass

            date_time = self.format_game_time_date(date + " " + time)
            unix_game_time = int(datetime.timestamp(date_time))

            game_links = game.find_all("a", class_="gameLink")
            for link in game_links:

                bet_type = link.find(
                    "span", class_="bettingType").getText().strip()

                url = link["href"]

                teams = link.find('div', id='runnerNames').find_all('li')
                away_team = teams[0].getText().strip().split(':')[0]
                home_team = teams[1].getText().strip().split(':')[0]

                game_id = self.generate_game_id(
                    unix_game_time,
                    self.name_and_code[home_team],
                    self.name_and_code[away_team])

                result = {
                    "book": "sports_interaction",
                    "game_id": game_id,
                    "league": "mlb",
                    "game_time": unix_game_time,
                }

                bet_contents = link.find_all('div', class_='betTypeContent')
                for bet_content in bet_contents:

                    bet_type = bet_content.find(
                        'span', class_='bettingType').getText().strip()
                    runners = bet_content.find_all('li', class_='runner')

                    away_odds_as_dec = runners[0].find(
                        "span", class_="addRunner")["data-runnerprice"]
                    home_odds_as_dec = runners[1].find(
                        "span", class_="addRunner")["data-runnerprice"]

                    isClosed = True if "closed" in runners[0]["title"] else False
                    if isClosed:
                        continue

                    home_odds = self.convert_odds(home_odds_as_dec)
                    away_odds = self.convert_odds(away_odds_as_dec)

                    # converts bet_type to common names and collects meta
                    if bet_type == "Runline":
                        bet_type = "Handicap"
                        meta = bet_content.find(
                            "span", class_="handicap").getText().strip()
                        # multiply by negative one to get the home team's meta
                        meta = -1 * float(meta)
                    elif bet_type == "Moneyline":
                        bet_type = "Match"
                        meta = None
                    elif bet_type == "Over/Under":
                        meta = bet_content.find(
                            "span", class_="handicap").getText().strip()
                        meta = float(meta)

                    details = {
                        "bet_type": bet_type,
                        "link": url,
                        "home_team": self.name_and_code[home_team],
                        "home_team_odds": home_odds,
                        "away_team": self.name_and_code[away_team],
                        "away_team_odds": away_odds,
                        "take": (home_odds + away_odds)
                    }

                    result = {
                        **result,
                        **details,
                        "meta": meta
                    }

                    results.append(result)
                    number_of_games += 1

        return results, number_of_games

    name_and_code = {"Arizona": "ARI",
                     "Milwaukee": "MIL",
                     "Miami": "MIA",
                     "Oakland": "OAK",
                     "Cleveland": "CLE",
                     "Boston": "BOS",
                     "San Diego": "SDN",
                     "St Louis": "SLN",
                     "Chicago (N)": "CHN",
                     "Seattle": "SEA",
                     "Kansas City": "KCA",
                     "Tampa Bay": "TBA",
                     "Colorado": "COL",
                     "Philadelphia": "PHI",
                     "New York (N)": "NYN",
                     "Houston": "HOU",
                     "Chicago (A)": "CHA",
                     "Baltimore": "BAL",
                     "Los Angeles": "LAN",
                     "Cincinnati": "CIN",
                     "Atlanta": "ATL",
                     "Los Angeles (A)": "ANA",
                     "Detroit": "DET",
                     "New York (A)": "NYA",
                     "San Francisco": "SFN",
                     "Pittsburgh": "PIT",
                     "Washington": "WAS",
                     "Texas": "TEX",
                     "Minnesota": "MIN",
                     "Toronto": "TOR"}
