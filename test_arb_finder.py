import json
import arb_finder
import utils


def test_nest_on_bet_type():
    with open("./test_data/test_exchanges.json") as json_file:
        data = json.load(json_file)
    returned = arb_finder.nest_on_bet_type(data)

    with open("./test_data/seperate_return.json") as json_file:
        expected = json.load(json_file)

    assert(returned == expected)


def test_cross_compare_results():
    # test simple case
    data = {
        "1563516600_CIN_SLN": [
            {'book': '888sport', 'game_id': '1563516600_CIN_SLN', 'league': 'mlb', 'game_time': 1563516600, 'bet_type': 'Match', 'link': "www.888sport.com/CIN_SLN", 'home_team': 'CIN',
             'home_team_odds': 0.64, 'away_team': 'SLN', 'away_team_odds': 0.42, 'take': 1.0530222693531281, 'meta': None},
            {'book': 'intertops', 'game_id': '1563516600_CIN_SLN', 'league': 'mlb', 'game_time': 1563516600, 'bet_type': 'Match', 'link': "www.intertops.com/CIN_SLN", 'home_team': 'CIN',
             'home_team_odds': 0.41, 'away_team': 'STL', 'away_team_odds': 0.6, 'take': 1.0216549391807124, 'meta': None}
        ]
    }
    returned, _ = arb_finder.cross_compare(data)
    print(returned)
    print(" '")
    expected = [
        {
            "game_id": "1563516600_CIN_SLN",
            "bet_type": "Match",
            "home_team_exchange": "intertops",
            "away_team_exchange": "888sport",
            "home_team": "CIN",
            "away_team": "SLN",
            "home_team_link": "www.intertops.com/CIN_SLN",
            "away_team_link": "www.888sport.com/CIN_SLN",
            "home_team_odds": 0.41,
            "away_team_odds": 0.42,
            "take": 0.83,
            "home_bet_ratio": 1.024390243902439,
            "away_bet_ratio": 1.0,
            "meta": None
        }
    ]
    print(expected)
    assert(returned == expected)
    # test case where meta doesnt match
    data = {
        "1563516600_CIN_SLN": [
            {'book': '888sport', 'game_id': '1563516600_CIN_SLN', 'league': 'mlb', 'game_time': 1563516600, 'bet_type': 'Hanicap', 'link': "www.888sport.com/CIN_SLN", 'home_team': 'CIN',
             'home_team_odds': 0.64, 'away_team': 'SLN', 'away_team_odds': 0.42, 'take': 1.0530222693531281, 'meta': 1.5},
            {'book': 'intertops', 'game_id': '1563516600_CIN_SLN', 'league': 'mlb', 'game_time': 1563516600, 'bet_type': 'Hanicap', 'link': "www.intertops.com/CIN_SLN", 'home_team': 'CIN',
             'home_team_odds': 0.41, 'away_team': 'STL', 'away_team_odds': 0.6, 'take': 1.0216549391807124, 'meta': -1.5}
        ]
    }
    returned, _ = arb_finder.cross_compare(data)
    expected = []

    assert(returned == expected)


def test_cross_compare_number():
    # test simple case
    data = {
        "1563516600_CIN_SLN": [
            {'book': '888sport', 'game_id': '1563516600_CIN_SLN', 'league': 'mlb', 'game_time': 1563516600, 'bet_type': 'Match', 'link': "www.888sport.com/CIN_SLN", 'home_team': 'CIN',
             'home_team_odds': 0.64, 'away_team': 'SLN', 'away_team_odds': 0.42, 'take': 1.0530222693531281, 'meta': None},
            {'book': 'intertops', 'game_id': '1563516600_CIN_SLN', 'league': 'mlb', 'game_time': 1563516600, 'bet_type': 'Match', 'link': "www.intertops.com/CIN_SLN", 'home_team': 'CIN',
             'home_team_odds': 0.41, 'away_team': 'STL', 'away_team_odds': 0.6, 'take': 1.0216549391807124, 'meta': None}
        ]
    }
    _, returned = arb_finder.cross_compare(data)
    expected = 2
    assert(returned == expected)
    # test case where meta doesnt match
    data = {
        "1563516600_CIN_SLN": [
            {'book': '888sport', 'game_id': '1563516600_CIN_SLN', 'league': 'mlb', 'game_time': 1563516600, 'bet_type': 'Hanicap', 'link': "www.888sport.com/CIN_SLN", 'home_team': 'CIN',
             'home_team_odds': 0.64, 'away_team': 'SLN', 'away_team_odds': 0.42, 'take': 1.0530222693531281, 'meta': 1.5},
            {'book': 'intertops', 'game_id': '1563516600_CIN_SLN', 'league': 'mlb', 'game_time': 1563516600, 'bet_type': 'Hanicap', 'link': "www.intertops.com/CIN_SLN", 'home_team': 'CIN',
             'home_team_odds': 0.41, 'away_team': 'STL', 'away_team_odds': 0.6, 'take': 1.0216549391807124, 'meta': -1.5}
        ]
    }
    _, returned = arb_finder.cross_compare(data)
    expected = 0

    assert(returned == expected)


def test_find_arb_opportunities():
    # load raw test data
    with open("./test_data/pipeline_test_exchanges.json") as json_file:
        exchanges = json.load(json_file)
    # process raw test data
    returned, _ = arb_finder.find_arb_opportunities(exchanges)
    # load expected data
    with open("./test_data/pipeline_test_expected.json") as json_file:
        expected = json.load(json_file)

    for r, e in zip(returned, expected):
        assert(r == e)


def test_find_arb_opportunities_schema():
    # load raw test data
    with open("./test_data/pipeline_test_exchanges.json") as json_file:
        exchanges = json.load(json_file)
    # process raw test data
    returned, _ = arb_finder.find_arb_opportunities(exchanges)

    utils.arb_opp_schema.validate(returned)


def test_find_arb_opportunities_number():
    # load raw test data
    with open("./test_data/pipeline_test_exchanges.json") as json_file:
        exchanges = json.load(json_file)
    # process raw test data
    _, returned = arb_finder.find_arb_opportunities(exchanges)

    assert(returned == 18)


def test_calculate_bet_on_arb():
    data = {
        "game_id": "1563516600_CIN_SLN",
        "bet_type": "Match",
        "home_team_exchange": "intertops",
        "away_team_exchange": "888sport",
        "home_team": "CIN",
        "away_team": "SLN",
        "home_team_odds": 0.31,
        "away_team_odds": 0.62,
        "take": 0.93
    }

    returned = arb_finder.calculate_bet_ratios(data)

    expected = {
        "home_bet_ratio": 2.0,
        "away_bet_ratio": 1.0,
    }

    assert(returned == expected)
