from intertops import Intertops
from sport888 import Sport888
from bodog import Bodog
from sports_interaction import Sports_Interaction
from bs4 import BeautifulSoup
import pickle
import utils
import dateutil
from datetime import datetime, time
from dateutil.parser import parse


def test_intertops_results():
    intertops = Intertops()

    # load test events
    path = "./test_data/"
    with open(path + "intertops_events.html", "r") as file:
        events = BeautifulSoup(file.read(), "html.parser")

    # tests functionality
    returned, _ = intertops.parse_events_for_odds(events)
    with open(path + "intertops_results.pkl", "rb") as file:
        expected = pickle.load(file)
    assert(returned == expected)


def test_intertops_shape():
    intertops = Intertops()

    # load test events
    path = "./test_data/"
    with open(path + "intertops_events.html", "r") as file:
        events = BeautifulSoup(file.read(), "html.parser")

    returned, _ = intertops.parse_events_for_odds(events)

    # tests shape of data
    utils.exchange_result_schema.validate(returned)


def test_intertops_time_conversion():
    intertops = Intertops()

    # load test events
    path = "./test_data/"
    with open(path + "intertops_events.html", "r") as file:
        events = BeautifulSoup(file.read(), "html.parser")

    returned = intertops.get_game_times(events)
    expected = datetime(2019, 7, 28, 0, 40, 0, 0, dateutil.tz.tzutc())

    assert(returned[0] == expected)


def test_intertops_number_of_games():
    intertops = Intertops()

    # load test events
    path = "./test_data/"
    with open(path + "intertops_events.html", "r") as file:
        events = BeautifulSoup(file.read(), "html.parser")

    _, returned = intertops.parse_events_for_odds(events)

    assert(returned == 9)


def test_sport888_results():
    sport888 = Sport888()

    # load test events
    path = "./test_data/"
    with open(path + "sport888_events.txt", "rb") as file:
        events = pickle.load(file)

    # tests functionality
    returned, _ = sport888.parse_events_for_odds(events)
    with open(path + "sport888_results.pkl", "rb") as file:
        expected = pickle.load(file)
    assert(returned == expected)


def test_sport888_shape():
    sport888 = Sport888()

    # load test events
    path = "./test_data/"
    with open(path + "sport888_events.txt", "rb") as file:
        events = pickle.load(file)

    returned, _ = sport888.parse_events_for_odds(events)

    # test shape of data
    utils.exchange_result_schema.validate(returned)


def test_sport888_time_conversion():
    sport888 = Sport888()

    returned = sport888.format_game_time_date("2019-07-28T00:40:00Z")

    expected = datetime(2019, 7, 28, 0, 40, 0, 0, dateutil.tz.tzutc())

    assert(returned == expected)


def test_sport888_number_of_games():
    sport888 = Sport888()

    # load test events
    path = "./test_data/"
    with open(path + "sport888_events.txt", "rb") as file:
        events = pickle.load(file)

    _, returned = sport888.parse_events_for_odds(events)
    assert(returned == 45)


def test_bodog_results():
    bodog = Bodog()

    # load test events
    path = "./test_data/"
    with open(path + "bodog_events.pkl", "rb") as file:
        events = pickle.load(file)

    returned, _ = bodog.parse_events_for_odds(events)

    with open(path + "bodog_results.pkl", "rb") as file:
        expected = pickle.load(file)

    assert(returned == expected)


def test_bodog_shape():
    bodog = Bodog()

    # load test events
    path = "./test_data/"
    with open(path + "bodog_events.pkl", "rb") as file:
        events = pickle.load(file)

    returned, _ = bodog.parse_events_for_odds(events)

    utils.exchange_result_schema.validate(returned)


def test_bodog_number_of_games():
    bodog = Bodog()

    # load test events
    path = "./test_data/"
    with open(path + "bodog_events.pkl", "rb") as file:
        events = pickle.load(file)

    _, returned = bodog.parse_events_for_odds(events)

    assert(returned == 17)


def test_sports_interaction_results():
    sports_interaction = Sports_Interaction()

    # load test events
    path = "./test_data/"
    with open(path + "sports_interaction_events.pkl", "rb") as file:
        events = pickle.load(file)

    returned, _ = sports_interaction.parse_events_for_odds(events)
    returned = returned[:7]

    with open(path + "sports_interaction_results.pkl", "rb") as file:
        expected = pickle.load(file)

    assert(returned == expected)


def test_sports_interaction_number_of_games():
    sports_interaction = Sports_Interaction()

    # load test events
    path = "./test_data/"
    with open(path + "sports_interaction_events.pkl", "rb") as file:
        events = pickle.load(file)

    _, returned = sports_interaction.parse_events_for_odds(events)

    assert(returned == 17)


def test_sports_interaction_shape():
    sports_interaction = Sports_Interaction()

    # load test events
    path = "./test_data/"
    with open(path + "sports_interaction_events.pkl", "rb") as file:
        events = pickle.load(file)

    returned, _ = sports_interaction.parse_events_for_odds(events)

    utils.exchange_result_schema.validate(returned)
