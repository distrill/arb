from main import arb_opp_string_builder, heart_beat_string_builder
import platform
import base_exchange
import utils


class ExchangeOne(base_exchange.Exchange):
    def __init__(self):
        self.string = "test exchange One"


class ExchangeTwo(base_exchange.Exchange):
    def __init__(self):
        self.string = "test exchange Two"


def test_output_string_builder_heart_beat_only_success():
    host_name = platform.node()
    games_compared = 10
    games_in_exchange = [5, 2]
    test_exchange_one = ExchangeOne()
    test_exchange_two = ExchangeTwo()
    successful_exchanges = [
        test_exchange_one,
        test_exchange_two
    ]
    unsuccessful_exchanges = []
    returned = heart_beat_string_builder(
        games_compared,
        games_in_exchange,
        successful_exchanges,
        unsuccessful_exchanges
    )

    assert("heart beat" in returned)
    assert("arb chances compared : 10" in returned)
    assert("ExchangeOne: 5" in returned)
    assert("ExchangeTwo: 2" in returned)
    assert("unsuccesful" not in returned)
    assert(host_name in returned)


def test_output_string_builder_heart_beat_with_failure():
    host_name = platform.node()
    games_compared = 0
    games_in_exchange = [5]
    test_exchange_one = ExchangeOne()
    test_exchange_two = ExchangeTwo()
    successful_exchanges = [
        test_exchange_one
    ]
    unsuccessful_exchanges = [test_exchange_two]
    returned = heart_beat_string_builder(
        games_compared,
        games_in_exchange,
        successful_exchanges,
        unsuccessful_exchanges
    )
    print(returned)
    assert("heart beat" in returned)
    assert("arb chances compared : 0" in returned)
    assert("ExchangeOne: 5" in returned)
    assert("unsuccesful exchanges : \nExchangeTwo" in returned)
    assert(host_name in returned)


def test_arb_opp_string_builder_with_alert():
    arb_opps = [
        {
            "game_id": "1563516600_CIN_SLN",
            "bet_type": "Match",
            "home_team_exchange": "intertops",
            "away_team_exchange": "888sport",
            "home_team": "CIN",
            "away_team": "SLN",
            "home_team_odds": 0.41,
            "away_team_odds": 0.42,
            "take": 0.95
        }
    ]
    alert = utils.make_channel_alert(arb_opps)
    returned = arb_opp_string_builder(
        arb_opps,

        alert
    )
    assert("{'game_id': '1563516600_CIN_SLN', 'bet_type': 'Match', 'home_team_exchange': 'intertops', 'away_team_exchange': '888sport', 'home_team': 'CIN', 'away_team': 'SLN', 'home_team_odds': 0.41, 'away_team_odds': 0.42, 'take': 0.95}" in returned)
    assert("<!channel>" in returned)


def test_arb_opp_string_builder_without_alert():
    host_name = platform.node()
    arb_opps = [
        {
            "game_id": "1563516600_CIN_SLN",
            "bet_type": "Match",
            "home_team_exchange": "intertops",
            "away_team_exchange": "888sport",
            "home_team": "CIN",
            "away_team": "SLN",
            "home_team_odds": 0.41,
            "away_team_odds": 0.42,
        },
        {
            "game_id": "13498571_ONE_TWO",
            "bet_type": "Match",
            "home_team_exchange": "ExchangeOne",
            "away_team_exchange": "ExchangeTwo",
            "home_team": "ONE",
            "away_team": "TWO",
            "home_team_odds": 0.41,
            "away_team_odds": 0.42,
        }
    ]
    returned = arb_opp_string_builder(
        arb_opps,
    )

    assert("{'game_id': '1563516600_CIN_SLN', 'bet_type': 'Match', 'home_team_exchange': 'intertops', 'away_team_exchange': '888sport', 'home_team': 'CIN', 'away_team': 'SLN', 'home_team_odds': 0.41, 'away_team_odds': 0.42}" in returned)
    assert("{'game_id': '13498571_ONE_TWO', 'bet_type': 'Match', 'home_team_exchange': 'ExchangeOne', 'away_team_exchange': 'ExchangeTwo', 'home_team': 'ONE', 'away_team': 'TWO', 'home_team_odds': 0.41, 'away_team_odds': 0.42}" in returned)
    assert(host_name in returned)
