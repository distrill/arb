import utils
import json
import schema


def test_fractional_to_implied():
    fraction = "29/20"
    returned = utils.fractional_to_implied(fraction)
    expected = 20.0 / (29 + 20)
    assert(returned == expected)


def test_decimal_to_implied():
    decimal_odds = 1.4
    returned = utils.decimal_to_implied(decimal_odds)
    expected = 1/1.4
    assert(returned == expected)

    decimal_odds = 1.85
    returned1 = utils.decimal_to_implied(decimal_odds)
    expected = 1/1.85
    assert(returned1 == expected)

    assert(returned + returned1 == ((1/1.4) + (1/1.85)))


def test_implied_to_decimal():
    implied_odds = 0.8771929824561404
    returned = utils.implied_to_decimal(implied_odds)
    expected = 1.14
    assert(returned == expected)

    implied_odds = 0.5405405405405405
    returned = utils.implied_to_decimal(implied_odds)
    expected = 1.85
    assert(round(returned, 4) == expected)


def test_filter_arb_opportunities():
    data = [
        {'game_id': '1567023000_SEA_NYA', 'bet_type': 'Handicap', 'home_team_exchange': 'bodog', 'away_team_exchange': '888sport', 'home_team': 'SEA', 'away_team': 'NYA',
            'home_team_odds': 0.3921568627450981, 'away_team_odds': 0.6, 'take': 0.9921568627450981, 'meta': 1.5, 'home_bet_ratio': 1.5299999999999998, 'away_bet_ratio': 1.0},
        {'game_id': '1567023000_SEA_NYA', 'bet_type': 'Handicap', 'home_team_exchange': 'bodog', 'away_team_exchange': 'intertops', 'home_team': 'SEA', 'away_team': 'NYY',
            'home_team_odds': 0.3921568627450981, 'away_team_odds': 0.5999880002399952, 'take': 0.9921448629850933, 'meta': 1.5, 'home_bet_ratio': 1.5299694006119875, 'away_bet_ratio': 1.0},
        {'game_id': '1567023000_SEA_NYA', 'bet_type': 'Handicap', 'home_team_exchange': 'testtest', 'away_team_exchange': 'thebook', 'home_team': 'SEA', 'away_team': 'NYY',
            'home_team_odds': 0.2921568627450981, 'away_team_odds': 0.5999880002399952, 'take': 0.8921448629850933, 'meta': 1.5, 'home_bet_ratio': 1.5299694006119875, 'away_bet_ratio': 1.0},
        {'game_id': '1567023000_SEA_NYA', 'bet_type': 'Handicap', 'home_team_exchange': 'exbook', 'away_team_exchange': 'testbook', 'home_team': 'SEA', 'away_team': 'NYY',
            'home_team_odds': 0.5, 'away_team_odds': 0.49, 'take': 0.99, 'meta': 1.5, 'home_bet_ratio': 1.5299694006119875, 'away_bet_ratio': 1.0},
    ]
    returned = utils.filter_arb_opportunities(data)
    expected = [{'game_id': '1567023000_SEA_NYA', 'bet_type': 'Handicap', 'home_team_exchange': 'testtest', 'away_team_exchange': 'thebook', 'home_team': 'SEA', 'away_team': 'NYY',
                 'home_team_odds': 0.2921568627450981, 'away_team_odds': 0.5999880002399952, 'take': 0.8921448629850933, 'meta': 1.5, 'home_bet_ratio': 1.5299694006119875, 'away_bet_ratio': 1.0},
                {'game_id': '1567023000_SEA_NYA', 'bet_type': 'Handicap', 'home_team_exchange': 'exbook', 'away_team_exchange': 'testbook', 'home_team': 'SEA', 'away_team': 'NYY',
                 'home_team_odds': 0.5, 'away_team_odds': 0.49, 'take': 0.99, 'meta': 1.5, 'home_bet_ratio': 1.5299694006119875, 'away_bet_ratio': 1.0},
                ]

    assert(returned == expected)


def test_make_channel_alert():
    data_with_alert = [
        {'game_id': '1567023000_SEA_NYA', 'bet_type': 'Handicap', 'home_team_exchange': 'bodog', 'away_team_exchange': '888sport', 'home_team': 'SEA', 'away_team': 'NYA',
         'home_team_odds': 0.3921568627450981, 'away_team_odds': 0.6, 'take': 0.9921568627450981, 'meta': 1.5, 'home_bet_ratio': 1.5299999999999998, 'away_bet_ratio': 1.0},
        {'game_id': '1567023000_SEA_NYA', 'bet_type': 'Handicap', 'home_team_exchange': 'bodog', 'away_team_exchange': 'intertops', 'home_team': 'SEA', 'away_team': 'NYY',
         'home_team_odds': 0.3921568627450981, 'away_team_odds': 0.5999880002399952, 'take': 0.9921448629850933, 'meta': 1.5, 'home_bet_ratio': 1.5299694006119875, 'away_bet_ratio': 1.0},
        {'game_id': '1567023000_SEA_NYA', 'bet_type': 'Handicap', 'home_team_exchange': 'testtest', 'away_team_exchange': 'thebook', 'home_team': 'SEA', 'away_team': 'NYY',
         'home_team_odds': 0.2921568627450981, 'away_team_odds': 0.5999880002399952, 'take': 0.8921448629850933, 'meta': 1.5, 'home_bet_ratio': 1.5299694006119875, 'away_bet_ratio': 1.0},
        {'game_id': '1567023000_SEA_NYA', 'bet_type': 'Handicap', 'home_team_exchange': 'exbook', 'away_team_exchange': 'testbook', 'home_team': 'SEA', 'away_team': 'NYY',
         'home_team_odds': 0.5, 'away_team_odds': 0.49, 'take': 0.99, 'meta': 1.5, 'home_bet_ratio': 1.5299694006119875, 'away_bet_ratio': 1.0},
    ]
    returned = utils.make_channel_alert(data_with_alert)
    expected = True
    assert(returned == expected)

    data_without_alert = [
        {'game_id': '1567023000_SEA_NYA', 'bet_type': 'Handicap', 'home_team_exchange': 'bodog', 'away_team_exchange': '888sport', 'home_team': 'SEA', 'away_team': 'NYA',
         'home_team_odds': 0.3921568627450981, 'away_team_odds': 0.6, 'take': 0.9921568627450981, 'meta': 1.5, 'home_bet_ratio': 1.5299999999999998, 'away_bet_ratio': 1.0},
        {'game_id': '1567023000_SEA_NYA', 'bet_type': 'Handicap', 'home_team_exchange': 'bodog', 'away_team_exchange': 'intertops', 'home_team': 'SEA', 'away_team': 'NYY',
         'home_team_odds': 0.3921568627450981, 'away_team_odds': 0.5999880002399952, 'take': 0.9921448629850933, 'meta': 1.5, 'home_bet_ratio': 1.5299694006119875, 'away_bet_ratio': 1.0},
    ]
    returned = utils.make_channel_alert(data_without_alert)
    expected = False
    assert(returned == expected)
