import pickle
import datetime
from schema import Schema, And, Use, Optional, Or

MIN_TAKE_THRESHOLD = 0.99
MIN_TAKE_ALERT_THRESHOLD = 0.97


exchange_result_schema = Schema([
    {
        'book': And(str, len),
        'game_id': And(str, len),
        'league': And(str, len),
        'game_time': int,
        'bet_type': lambda n: n in set(["Match", "Handicap", "Over/Under"]),
        'link':Or(str, None),
        'home_team': Or(And(str, lambda n: n in set(["Over", "Under"])),
                        And(str, lambda n: len(n) == 3)),
        'home_team_odds': And(float, lambda n: 0 < n < 1),
        'away_team': Or(And(str, lambda n: n in set(["Over", "Under"])),
                        And(str, lambda n: len(n) == 3)),
        'away_team_odds': And(float, lambda n: 0 < n < 1),
        'take': float,
        'meta': Or(float, None)
    }
])

arb_opp_schema = Schema([
    {
        'game_id': And(str, len),
        'bet_type': lambda n: n in set(["Match", "Handicap", "Over/Under"]),
        "home_team_exchange": And(str, len),
        "away_team_exchange": And(str, len),
        "home_team": Or(And(str, lambda n: n in set(["Over", "Under"])),
                        And(str, lambda n: len(n) == 3)),
        "away_team": Or(And(str, lambda n: n in set(["Over", "Under"])),
                        And(str, lambda n: len(n) == 3)),
        "home_team_link": Or(str, None),
        "away_team_link": Or(str, None),
        "home_team_odds": And(float, lambda n: 0 < n < 1),
        "away_team_odds": And(float, lambda n: 0 < n < 1),
        "take": float,
        "meta": Or(float, None),
        "home_bet_ratio": float,
        "away_bet_ratio": float
    }
])


def fractional_to_implied(frac):
    """
    frac is a string like "20/29"
    returns a float between 0 and 1
    """
    numbers = [int(x) for x in frac.split("/")]
    implied = numbers[1] / (numbers[0] + numbers[1])
    return implied


def decimal_to_implied(input):
    """
    converts decimal odds to implied probabillity
    input is a float like 1.40
    returns a float between 0 and 1
    """
    return 1/input


def implied_to_decimal(input):
    return 1/input


def list_to_unix(l):
    unix_times = []
    for item in l:
        date_time = int(datetime.datetime.timestamp(item))
        unix_times.append(date_time)
    return unix_times


def save_pickle(obj, path):

    # you need the below code if you run into a recursion depth error
    import resource
    import sys
    import utils
    max_rec = 0x100000

    # May segfault without this line. 0x100 is a guess at the size of each stack frame.
    resource.setrlimit(resource.RLIMIT_STACK, [
                       0x100 * max_rec, resource.RLIM_INFINITY])
    sys.setrecursionlimit(max_rec)
    """
    to easily save testing data
    """
    filehandler = open(path, "wb")
    pickle.dump(obj, filehandler)


def filter_arb_opportunities(arb_opportunities):
    filtered = []
    for arb_opportunity in arb_opportunities:
        if arb_opportunity["take"] <= MIN_TAKE_THRESHOLD:
            filtered.append(arb_opportunity)
    return filtered


def make_channel_alert(arb_opportunities):
    for arb_opportunity in arb_opportunities:
        if arb_opportunity["take"] <= MIN_TAKE_ALERT_THRESHOLD:
            return True
    return False


name_and_code = {"Arizona Diamondbacks": "ARI",
                 "Milwaukee Brewers": "MIL",
                 "Miami Marlins": "MIA",
                 "Oakland Athletics": "OAK",
                 "Cleveland Indians": "CLE",
                 "Boston Red Sox": "BOS",
                 "San Diego Padres": "SDN",
                 "St. Louis Cardinals": "SLN",
                 "Chicago Cubs": "CHN",
                 "Seattle Mariners": "SEA",
                 "Kansas City Royals": "KCA",
                 "Tampa Bay Rays": "TBA",
                 "Colorado Rockies": "COL",
                 "Philadelphia Phillies": "PHI",
                 "New York Mets": "NYN",
                 "Houston Astros": "HOU",
                 "Chicago White Sox": "CHA",
                 "Baltimore Orioles": "BAL",
                 "Los Angeles Dodgers": "LAN",
                 "Cincinnati Reds": "CIN",
                 "Atlanta Braves": "ATL",
                 "Los Angeles Angels": "ANA",
                 "Detroit Tigers": "DET",
                 "New York Yankees": "NYA",
                 "San Francisco Giants": "SFN",
                 "Pittsburgh Pirates": "PIT",
                 "Washington Nationals": "WAS",
                 "Texas Rangers": "TEX",
                 "Minnesota Twins": "MIN",
                 "Toronto Blue Jays": "TOR"}
